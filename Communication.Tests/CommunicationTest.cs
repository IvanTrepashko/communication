using Communication.Implementations;
using NUnit.Framework;
using Communication.Interfaces;
using Microsoft.Extensions.Logging;

namespace Communication.Tests
{
    public class CommunicationTest
    {
        private CommunicationSystem system;

        private IReadMessage reader;
        private ISendMessage sender;
        private Messenger messenger;
        
        [SetUp]
        public void Setup()
        {
            var logger = LoggerFactory.Create(x => x.AddConsole()).CreateLogger("ConsoleLogger");
            this.system = new CommunicationSystem(logger);

            this.reader = new MessageReader(this.system, "Message Reader");
            this.sender = new MessageSender(this.system, "Message Sender");
            this.messenger = new Messenger(this.system, "Messenger");
        }

        [Test]
        public void SendMessages_Test()
        {
            string[] messages = {"Message1", "Message2"};

            foreach (var message in messages)
            {
                this.sender.SendMessage(message, this.reader);
            }
            
            Assert.AreEqual(messages[0], reader.ReadMessage().Text);
            Assert.AreEqual(messages[1], reader.ReadMessage().Text);
        }

        [Test]
        public void SendMessagesToAll_Test()
        {
            string message = "Message to All";
            this.messenger.SendMessageToAll(message);
            
            Assert.AreEqual(message, this.reader.ReadMessage().Text);
        }

        [Test]
        public void SendMessageToItself_Test()
        {
            string message = "Message to myself";

            this.messenger.SendMessage(message, this.messenger);
            
            Assert.AreEqual(message, this.messenger.ReadMessage().Text);
        }
    }
}
﻿using System;
using Communication;
using Communication.Implementations;
using Communication.Interfaces;
using Microsoft.Extensions.Logging;

namespace CommunicationConsole
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var factory = LoggerFactory.Create(x => x.AddConsole());

            var logger = factory.CreateLogger("ConsoleLogger");
            
            CommunicationSystem system = new CommunicationSystem(logger);

            IReadMessage reader = new MessageReader(system, "Message Reader");
            ISendMessage sender = new MessageSender(system, "Message Sender");

            Messenger messenger = new Messenger(system, "Messenger");

            messenger.SendMessage("Message from messenger", reader);
            
            sender.SendMessage("First message", reader);
            sender.SendMessage("Second message", messenger);
            
            Console.WriteLine(reader.ReadMessage().Text);
            Console.WriteLine(messenger.ReadMessage().Text);
            
            messenger.SendMessageToAll("MESSAGE TO ALL");
        }
    }
}
﻿using System;
using Communication.Interfaces;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using Communication.CommunicationEventArgs;

namespace Communication
{
    /// <summary>
    /// Represents class for exchanging messages.
    /// </summary>
    public class CommunicationSystem
    {
        private readonly Dictionary<string, Participant> participants;
        private readonly ILogger logger;
        
        /// <summary>
        /// Creates new instance of <see cref="CommunicationSystem"/> class.
        /// </summary>
        /// <param name="logger">Logger.</param>
        public CommunicationSystem(ILogger logger)
        {
            this.participants = new Dictionary<string, Participant>();
            this.logger = logger;
        }

        /// <summary>
        /// Registers new participant.
        /// </summary>
        /// <param name="participant">New participant.</param>
        /// <exception cref="ArgumentNullException">Thrown when participant is null.</exception>
        public void RegisterParticipant(Participant participant)
        {
            if (participant is null)
            {
                throw new ArgumentNullException(nameof(participant));
            }

            if (this.participants.ContainsKey(participant.Name))
            {
                this.logger.LogInformation("This participant is already registered");
                return;
            }

            this.participants.Add(participant.Name, participant);

            if (participant is IReadMessage r)
            {
                r.MessageRead += HandleMessageRead;
                r.MessageReceived += HandleMessageReceived;
            }

            if (participant is ISendMessage s)
            {
                s.MessageSent += HandleMessageSent;
                s.MessageSentToAll += HandleMessageSentToAll;
            }
        }

        /// <summary>
        /// Unregisters participant from communication.
        /// </summary>
        /// <param name="participant">Participant to unregister.</param>
        /// <exception cref="ArgumentNullException">Thrown when participant is null.</exception>
        public void UnregisterParticipant(Participant participant)
        {
            if (participant is null)
            {
                throw new ArgumentNullException(nameof(participant));
            }

            this.participants.Remove(participant.Name);
            
            if (participant is IReadMessage r)
            {
                r.MessageRead -= HandleMessageRead;
                r.MessageReceived -= HandleMessageReceived;
            }

            if (participant is ISendMessage s)
            {
                s.MessageSent -= HandleMessageSent;
                s.MessageSentToAll -= HandleMessageSentToAll;
            }
        }

        private void HandleMessageReceived(object sender, EventArgs e)
        {
            this.logger.LogInformation("Message received by " + ((Participant)sender).Name);
        }

        private void HandleMessageRead(object sender, EventArgs e)
        {
            this.logger.LogInformation("Message read by " + ((Participant)sender).Name);
        }
        
        private void HandleMessageSent(object sender, MessageSentEventArgs e)
        {
            if (this.participants.All(x => x.Key == e.Message.RecipientName))
            {
                this.logger.LogInformation($"Recipient with name {e.Message.RecipientName} is not registered.");
                return;
            }
            
            this.logger.LogInformation($"{e.Message.SenderName} send message to {e.Message.RecipientName}");
            ((IReadMessage)this.participants[e.Message.RecipientName]).AddUnreadMessage(e.Message);
        }

        private void HandleMessageSentToAll(object sender, MessageToAllEventHandler e)
        {
            ISendMessage messageSender = (ISendMessage) sender;

            this.logger.LogInformation($"{messageSender.Name} send message to all participants.");

            foreach (var participant in participants)
            {
                if (participant.Value is IReadMessage r && !ReferenceEquals(messageSender, participant.Value))
                {
                    r.AddUnreadMessage(new Message(e.Message, messageSender.Name, participant.Key));
                }
            }
        }
    }
}
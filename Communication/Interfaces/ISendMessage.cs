﻿using System;
using Communication.CommunicationEventArgs;

namespace Communication.Interfaces
{
    /// <summary>
    /// Provides methods for sending messages.
    /// </summary>
    public interface ISendMessage
    {
        string Name { get; }
        
        /// <summary>
        /// Occurs when message is sent.
        /// </summary>
        event EventHandler<MessageSentEventArgs> MessageSent;

        /// <summary>
        /// Occurs when message is sent to all.
        /// </summary>
        event EventHandler<MessageToAllEventHandler> MessageSentToAll;
        
        /// <summary>
        /// Sends message to specified recipient.
        /// </summary>
        /// <param name="message">Message.</param>
        /// <param name="recipient">Recipient.</param>
        void SendMessage(string message, IReadMessage recipient);

        /// <summary>
        /// Sends message to all recipients.
        /// </summary>
        /// <param name="message"></param>
        void SendMessageToAll(string message);
    }
}
﻿using System;
using System.Collections.Generic;

namespace Communication.Interfaces
{
    /// <summary>
    /// Provides methods for reading messages.
    /// </summary>
    public interface IReadMessage
    {
        string Name { get; }
        
        /// <summary>
        /// Occurs when message is received.
        /// </summary>
        event EventHandler MessageReceived;
        
        /// <summary>
        /// Occurs when message is read.
        /// </summary>
        event EventHandler MessageRead;
        
        /// <summary>
        /// Reads message.
        /// </summary>
        /// <returns>Message.</returns>
        Message ReadMessage();

        /// <summary>
        /// Adds new unread message.
        /// </summary>
        /// <param name="message">Unread message.</param>
        void AddUnreadMessage(Message message);
    }
}
﻿namespace Communication.Interfaces
{
    public abstract class Participant
    {
        public string Name { get; }

        protected Participant(CommunicationSystem system, string name)
        {
            this.Name = name;
            system.RegisterParticipant(this);
        }
    }
}
﻿using System;
using Communication.Interfaces;
using System.Collections.Generic;

namespace Communication.Implementations
{
    /// <summary>
    /// Represents class for reading messages.
    /// </summary>
    public class MessageReader : Participant, IReadMessage
    {
        private readonly Queue<Message> unreadMessages;

        /// <summary>
        /// Creates new instance of <see cref="MessageReader"/> class.
        /// </summary>
        /// <param name="system">Communication system.</param>
        /// <param name="name">Name of participant.</param>
        public MessageReader(CommunicationSystem system, string name) : base(system, name)
        {
            this.unreadMessages = new Queue<Message>();
        }

        /// <inheritdoc cref="IReadMessage"/>
        public event EventHandler MessageReceived;
        
        /// <inheritdoc cref="IReadMessage"/>
        public event EventHandler MessageRead;
        
        /// <inheritdoc cref="IReadMessage"/>
        public Message ReadMessage()
        {
            if (this.unreadMessages.Count != 0)
            {
                this.OnMessageRead();
                return this.unreadMessages.Dequeue();
            }

            return new Message();
        }
        
        /// <inheritdoc cref="IReadMessage"/>
        public void AddUnreadMessage(Message message)
        {
            this.unreadMessages.Enqueue(message);
            this.OnMessageReceived();
        }
        
        protected virtual void OnMessageRead()
        {
            this.MessageRead?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnMessageReceived()
        {
            this.MessageReceived?.Invoke(this, EventArgs.Empty);
        }
    }
}
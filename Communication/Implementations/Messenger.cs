﻿using System;
using Communication.Interfaces;
using System.Collections.Generic;
using Communication.CommunicationEventArgs;

namespace Communication.Implementations
{
    /// <summary>
    /// Represents class for sending and reading messages.
    /// </summary>
    public class Messenger : Participant, IReadMessage, ISendMessage
    {
        private Queue<Message> unreadMessages;

        /// <inheritdoc cref="IReadMessage"/>
        public event EventHandler MessageReceived;
        
        /// <inheritdoc cref="IReadMessage"/>
        public event EventHandler MessageRead;
        
        /// <inheritdoc cref="ISendMessage"/>
        public event EventHandler<MessageSentEventArgs> MessageSent;
        
        /// <inheritdoc cref="ISendMessage"/>
        public event EventHandler<MessageToAllEventHandler> MessageSentToAll;

        /// <summary>
        /// Creates new instance of <see cref="Messenger"/> class.
        /// </summary>
        /// <param name="system">Communication system.</param>
        /// <param name="name">Name.</param>
        public Messenger(CommunicationSystem system, string name) : base(system, name)
        {
            this.unreadMessages = new Queue<Message>();
        }

        /// <inheritdoc cref="IReadMessage"/>
        public Message ReadMessage()
        {
            if (this.unreadMessages.Count != 0)
            {
                this.OnMessageRead();
                return this.unreadMessages.Dequeue();
            }

            return null;
        }

        /// <inheritdoc cref="IReadMessage"/>
        public void AddUnreadMessage(Message message)
        {
            this.unreadMessages.Enqueue(message);
            this.OnMessageReceived();
        }

        /// <inheritdoc cref="ISendMessage"/>
        public void SendMessage(string message, IReadMessage recipient)
        {
            if (recipient is null)
            {
                throw new ArgumentNullException(nameof(recipient));
            }
            
            this.OnMessageSent(message, recipient);
        }

        /// <inheritdoc cref="ISendMessage"/>
        public void SendMessageToAll(string message)
        {
            this.OnMessageSentToAll(message);
        }

        protected virtual void OnMessageSent(string message, IReadMessage recipient)
        {
            this.MessageSent?.Invoke(this, new MessageSentEventArgs(new Message(message, this.Name, recipient.Name)));
        }
        
        protected virtual void OnMessageRead()
        {
            this.MessageRead?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnMessageReceived()
        {
            this.MessageReceived?.Invoke(this, EventArgs.Empty);
        }
        
        protected virtual void OnMessageSentToAll(string message)
        {
            this.MessageSentToAll?.Invoke(this, new MessageToAllEventHandler(message));
        }
    }
}
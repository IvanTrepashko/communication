﻿using System;
using Communication.Interfaces;
using Communication.CommunicationEventArgs;

namespace Communication.Implementations
{
    /// <summary>
    /// Represents class for sending messages.
    /// </summary>
    public class MessageSender : Participant, ISendMessage
    {
        /// <inheritdoc cref="ISendMessage"/>
        public event EventHandler<MessageSentEventArgs> MessageSent;

        /// <inheritdoc cref="ISendMessage"/>
        public event EventHandler<MessageToAllEventHandler> MessageSentToAll;

        /// <summary>
        /// Creates new instance of <see cref="MessageSender"/> class.
        /// </summary>
        /// <param name="system">Communication system.</param>
        /// <param name="name">Name of participant.</param>
        public MessageSender(CommunicationSystem system, string name) : base(system, name)
        {
        }

        /// <inheritdoc cref="ISendMessage"/>
        public void SendMessage(string message, IReadMessage recipient)
        {
            if (recipient is null)
            {
                throw new ArgumentNullException(nameof(recipient));
            }
            
            this.OnMessageSent(message, recipient);
        }

        /// <inheritdoc cref="ISendMessage"/>
        public void SendMessageToAll(string message)
        {
            this.OnMessageSentToAll(message);
        }

        protected virtual void OnMessageSentToAll(string message)
        {
            this.MessageSentToAll?.Invoke(this, new MessageToAllEventHandler(message));
        }

        protected virtual void OnMessageSent(string message, IReadMessage recipient)
        {
            this.MessageSent?.Invoke(this, new MessageSentEventArgs(new Message(message, this.Name, recipient.Name)));
        }
    }
}
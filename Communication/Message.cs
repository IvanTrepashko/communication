﻿namespace Communication
{
    public class Message
    {
        public string Text { get; }
        
        public string SenderName { get; }
        
        public string RecipientName { get; }

        public Message()
        {
            this.Text = string.Empty;
            this.SenderName = string.Empty;
            this.RecipientName = string.Empty;
        }
        
        public Message(string text, string senderName, string recipientName)
        {
            Text = text;
            SenderName = senderName;
            RecipientName = recipientName;
        }
    }
}
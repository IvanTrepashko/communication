﻿using System;
using Communication.Interfaces;

namespace Communication.CommunicationEventArgs
{
    /// <summary>
    /// Represents event arguments for MessageSent event.
    /// </summary>
    public class MessageSentEventArgs : EventArgs
    {
        public Message Message { get; }

        public MessageSentEventArgs(Message message)
        {
            Message = message;
        }
    }
}
﻿namespace Communication.CommunicationEventArgs
{
    /// <summary>
    /// Represents event arguments for MessageToAll event.
    /// </summary>
    public class MessageToAllEventHandler
    {
        /// <summary>
        /// Gets message.
        /// </summary>
        public string Message { get; }

        /// <summary>
        /// Creates new instance of <see cref="MessageToAllEventHandler"/> class.
        /// </summary>
        /// <param name="message">Message.</param>
        public MessageToAllEventHandler(string message)
        {
            Message = message;
        }
    }
}